
# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths = {}
  str.split.each { |word| word_lengths[word] = word.length }
  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_k, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |key, value| older[key] = value }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count = Hash.new(0)
  word.downcase.each_char { |chr| letter_count[chr] += 1 }
  letter_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = Hash.new(0)
  arr.each { |el| uniq_hash[el] += 1 }
  uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity_count = { even: 0, odd: 0 }
  numbers.each do |number|
    parity_count[:even] += 1 if number.even?
    parity_count[:odd] += 1 if number.odd?
  end
  parity_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels_only = string.delete('qwrtypsdfghjklzxcvbnm ').split('').sort.reverse
  letter_count = Hash.new(0)
  vowels_only.each { |char| letter_count[char] += 1 }
  most_frequent = letter_count.sort_by { |_key, value| value }[-1]
  most_frequent[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  bday_kids = students.select { |_k, v| v > 6 }.keys
  results = []
  until bday_kids.length == 1
    idx = 1
    while idx < bday_kids.length
      results << [bday_kids[0], bday_kids[idx]]
      idx += 1
    end
    bday_kids.delete_at(0)
  end
  results
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_count = Hash.new(0)
  specimens.each { |animal| species_count[animal] += 1 }
  sorted_pop_sizes = species_count.values.sort
  (species_count.length**2) * (sorted_pop_sizes[0] / sorted_pop_sizes[-1])
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  n_sign_hash = character_count(normal_sign.downcase.delete(' ,.;:!?'))
  v_sign_hash = character_count(vandalized_sign.downcase.delete(' ,.;:!?'))
  result = true
  v_sign_hash.each do |char, quantity|
    result = false if n_sign_hash[char] < quantity
  end
  result
end

def character_count(str)
  char_hash = Hash.new(0)
  str.each_char { |char| char_hash[char] += 1 }
  char_hash
end
